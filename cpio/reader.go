package cpio

import (
	"errors"
	"io"
	"io/ioutil"
	"os"
	"strconv"
	"time"
)

type Reader struct {
	r   io.Reader
	err error
	nb  int64 // number of unread bytes for current of file entry
	pad int64 // amount of padding (ignored) after current file entry
}

func NewReader(r io.Reader) *Reader { return &Reader{r: r} }

var (
	ErrHeader = errors.New("archive/cpio: invalid cpio header")
)

func (tr *Reader) Next() (*Header, error) {
	var hdr *Header
	if tr.err == nil {
		tr.skipUnread()
	}
	//	tr.skipPadding(4)
	if tr.err == nil {
		hdr = tr.readHeader()
	}

	return hdr, tr.err
}

// cString parses bytes as a NUL-terminated C-style string.
// If a NUL byte is not found then the whole slice is returned as a string.
func cString(b []byte) string {
	n := 0
	for n < len(b) && b[n] != 0 {
		n++
	}
	return string(b[0:n])
}

// skipUnread skips any unread bytes in the existing file entry, as well as any alignment padding.
func (tr *Reader) skipUnread() {
	nr := tr.nb + tr.pad // number of bytes to skip
	tr.nb, tr.pad = 0, 0
	if sr, ok := tr.r.(io.Seeker); ok {
		if _, err := sr.Seek(nr, os.SEEK_CUR); err == nil {
			return
		}
	}
	_, tr.err = io.CopyN(ioutil.Discard, tr.r, nr)
}

func (tr *Reader) readHeader() *Header {
	magic := make([]byte, 6)
	if _, tr.err = io.ReadFull(tr.r, magic); tr.err != nil {
		return nil
	}

	var format string
	var header []byte
	switch string(magic) {
	case "070701": // new ASCII format
		format = "newc"
		header = make([]byte, newcHeaderSize-len(magic))
	case "070707": // old binary format
		format = "oldc"
		header = make([]byte, oldcHeaderSize-len(magic))
	case "070702": // crc format
		format = "crc"
		header = make([]byte, crcHeaderSize-len(magic))
	default:
		tr.err = ErrHeader
		return nil
	}
	if _, tr.err = io.ReadFull(tr.r, header); tr.err != nil {
		return nil
	}

	hdr := new(Header)
	s := slicer(header)
	switch format {
	case "newc":
		hdr.Magic = magic
		s.next(8) // skip ino
		mode := tr.int(s.next(8), 16, 64)
		//mode := tr.octal(s.next(8))
		hdr.Mode = mode & 0xFFF
		hdr.Uid = int(tr.int(s.next(8), 16, 64))
		hdr.Gid = int(tr.int(s.next(8), 16, 64))
		hdr.Nlink = int(tr.int(s.next(8), 16, 64))
		hdr.ModTime = time.Unix(tr.int(s.next(8), 16, 64), 0)
		hdr.Size = tr.int(s.next(8), 16, 64)
		hdr.Devmajor = tr.int(s.next(8), 16, 64)
		hdr.Devminor = tr.int(s.next(8), 16, 64)
		hdr.RDevmajor = tr.int(s.next(8), 16, 64)
		hdr.RDevminor = tr.int(s.next(8), 16, 64)
		hdr.Typeflag = (mode >> 12) & 0xF
		//		if hdr.Typeflag == TypeChar || hdr.Typeflag == TypeBlock {
		//			hdr.Devmajor = tr.int(devmajor, 16, 64)
		//			hdr.Devminor = tr.int(devminor, 16, 64)
		//		}

		namelen := int(tr.int(s.next(8), 16, 64))
		s.next(8) // checksum
		if x := (newcHeaderSize + namelen) % 4; x != 0 {
			namelen += (4 - (newcHeaderSize+namelen)%4) % 4
		}
		name := make([]byte, namelen)
		if _, tr.err = io.ReadFull(tr.r, name); tr.err != nil {
			return nil
		}
		hdr.Name = cString(name)
	}
	if hdr.Name == "TRAILER!!!" && hdr.Size == 0 {
		tr.err = io.EOF
		return hdr
	}
	if tr.err != nil {
		return nil
	}
	tr.nb = int64(hdr.Size)
	if x := hdr.Size % 4; x != 0 && hdr.Size != 0 {
		tr.pad += 4 - x
	} else {
		tr.pad = 0
	}
	return hdr
}

func (tr *Reader) int(b []byte, base int, bitsize int) int64 {
	x, err := strconv.ParseInt(string(b), base, bitsize)
	if err != nil {
		tr.err = err
	}
	return int64(x)
}

func (tr *Reader) octal(b []byte) int64 {
	// Check for binary format first.
	if len(b) > 0 && b[0]&0x80 != 0 {
		var x int64
		for i, c := range b {
			if i == 0 {
				c &= 0x7f // ignore signal bit in first byte
			}
			x = x<<8 | int64(c)
		}
		return x
	}

	// Removing leading spaces.
	for len(b) > 0 && b[0] == ' ' {
		b = b[1:]
	}
	// Removing trailing NULs and spaces.
	for len(b) > 0 && (b[len(b)-1] == ' ' || b[len(b)-1] == '\x00') {
		b = b[0 : len(b)-1]
	}
	x, err := strconv.ParseUint(cString(b), 8, 64)
	if err != nil {
		tr.err = err
	}
	return int64(x)
}

// Read reads from the current entry in the tar archive.
// It returns 0, io.EOF when it reaches the end of that entry,
// until Next is called to advance to the next entry.
func (tr *Reader) Read(b []byte) (n int, err error) {
	if tr.nb == 0 {
		// file consumed
		return 0, io.EOF
	}

	if int64(len(b)) > tr.nb {
		b = b[0:tr.nb]
	}
	n, err = tr.r.Read(b)
	tr.nb -= int64(n)

	if err == io.EOF && tr.nb > 0 {
		err = io.ErrUnexpectedEOF
	}
	tr.err = err
	return
}
