package cpio

import (
	"bytes"
	_ "fmt"
	"io"
	"os"
	"testing"
)

func TestSmall(t *testing.T) {
	f, err := os.Open("testdata/test-small.cpio")
	if err != nil {
		t.Fatalf(err.Error())
	}
	defer f.Close()
	r := NewReader(f)
	//buf := bytes.NewBuffer(nil)
	//	w := NewWriter(buf)
	//	var i int64
	for {
		hdr, err := r.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			//			log.Printf("%+v\n", h)
			t.Fatalf(err.Error())
		}
		//		i += 1

		switch hdr.Typeflag {
		default:

			//fi := hdr.FileInfo()
			//fmt.Printf("%+s\n", fi.Mode())
			//			fmt.Printf("%+v\n", hdr)
			if hdr.Name == "test.txt" {
				buf := bytes.NewBuffer(nil)
				_, err := io.Copy(buf, r)
				if err != nil {
					t.Fatalf(err.Error())
				}
				if buf.String() != "test\n" {
					t.Fatalf("data error: %s != %s\n", buf.String(), "test")
				}
			}

		}

	}

}

func TestBig(t *testing.T) {
	f, err := os.Open("testdata/test-big.cpio")
	if err != nil {
		t.Fatalf(err.Error())
	}
	defer f.Close()
	r := NewReader(f)
	//buf := bytes.NewBuffer(nil)
	//      w := NewWriter(buf)
	var i int64
	for {
		hdr, err := r.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			//                      log.Printf("%+v\n", h)
			t.Fatalf(err.Error())
		}
		i += 1

		switch hdr.Typeflag {
		default:
			//			if hdr.Size != 0 {
			//		fmt.Printf("%+v\n", hdr)
			//				buf := bytes.NewBuffer(nil)
			//				_, err := io.Copy(buf, r)
			//				if err != nil {
			//					t.Fatalf(err.Error())
			//				}
			//				if buf.String() != "test\n" {
			//					t.Fatalf("data error: %s != %s\n", buf.String(), "test")
			//				}
			//			}

		}

	}
	if i != 1318 {
		t.Fatalf("file count mismatch %d != %d\n", i, 1318)
	}
}
