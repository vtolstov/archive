package cpio

import (
	"errors"
	"fmt"
	"io"
	"strconv"
)

var (
	ErrWriteTooLong    = errors.New("archive/tar: write too long")
	ErrFieldTooLong    = errors.New("archive/tar: header field too long")
	ErrWriteAfterClose = errors.New("archive/tar: write after close")
)

// A Writer provides sequential writing of a tar archive in POSIX.1 format.
// A tar archive consists of a sequence of files.
// Call WriteHeader to begin a new file, and then call Write to supply that file's data,
// writing at most hdr.Size bytes in total.
type Writer struct {
	w      io.Writer
	err    error
	inode  int64
	length int64
	nb     int64 // number of unwritten bytes for current file entry
	closed bool
}

// NewWriter creates a new Writer writing to w.
func NewWriter(w io.Writer) *Writer { return &Writer{w: w, inode: 721} }

/*
// Flush finishes writing the current file (optional).
func (tw *Writer) Flush() error {
	if tw.nb > 0 {
		tw.err = fmt.Errorf("archive/cpio: missed writing %d bytes", tw.nb)
		return tw.err
	}

	n := tw.nb + tw.pad
	for n > 0 && tw.err == nil {
		nr := n
		if nr > blockSize {
			nr = blockSize
		}
		var nw int
		nw, tw.err = tw.w.Write(zeroBlock[0:nr])
		n -= int64(nw)
	}
	tw.nb = 0
	tw.pad = 0
	return tw.err
}
*/
// Write s into b, terminating it with a NUL if there is room.
func (tw *Writer) cString(b []byte, s string) {
	if len(s) > len(b) {
		if tw.err == nil {
			tw.err = ErrFieldTooLong
		}
		return
	}
	copy(b, s)
	if len(s) < len(b) {
		b[len(s)] = 0
	}
}

// Write x into b, either as octal or as binary (GNUtar/star extension).
func (tw *Writer) integer(b []byte, x int64) {
	s := strconv.FormatInt(x, 16)
	// leading zeros, but leave room for a NUL.
	for len(s)+1 < len(b) {
		s = "0" + s
	}
	tw.cString(b, s)
}

// WriteHeader writes hdr and prepares to accept the file's contents.
// WriteHeader calls Flush if it is not the first header.
// Calling after a Close will return ErrWriteAfterClose.
func (tw *Writer) WriteHeader(hdr *Header) error {
	var n int
	if tw.closed {
		return ErrWriteAfterClose
	}
	if tw.err == nil {
		//	tw.Flush()
	}
	if tw.err != nil {
		return tw.err
	}
	skip := (4 - (tw.length % 4)) % 4
	var buf []byte
	for ; skip > 0; skip-- {
		buf = append(buf, 0)
	}
	//	fmt.Printf("gg %+v\n", buf)
	n, tw.err = tw.w.Write(buf)
	tw.length += int64(n)
	buf = nil
	if tw.err != nil {
		return tw.err
	}

	nlinks := 1
	if hdr.Typeflag == TypeDir {
		nlinks = 2
	}
	name := []byte(hdr.Name)
	//	header := make([]byte, 110)
	/*	s := slicer(header)
		copy(s.next(6), hdr.Magic)
		tw.integer(s.next(8), tw.inode)
		tw.integer(s.next(8), int64(hdr.Mode&0xFFF|((hdr.Typeflag&0xF)<<12)))
		tw.integer(s.next(8), int64(hdr.Uid))
		tw.integer(s.next(8), int64(hdr.Gid))
		tw.integer(s.next(8), int64(nlinks))
		tw.integer(s.next(8), hdr.ModTime.Unix())
		tw.integer(s.next(8), hdr.Size)
		tw.integer(s.next(8), 3)
		tw.integer(s.next(8), 1)
		tw.integer(s.next(8), int64(hdr.Devmajor))
		tw.integer(s.next(8), int64(hdr.Devminor))
		tw.integer(s.next(8), int64(len(hdr.Name)+1))
		tw.integer(s.next(8), int64(0)) // checksumm
	*/
	mtime := hdr.ModTime.Unix()
	if mtime == -62135596800 {
		mtime = 0
	}
	shdr := fmt.Sprintf("%s%08X%08X%08X%08X%08X%08X%08X%08X%08X%08X%08X%08X%08X",
		"070701",
		tw.inode,
		int64(hdr.Mode&0xFFFF|((hdr.Typeflag&0xFFF)<<12)),
		hdr.Uid,
		hdr.Gid,
		nlinks,
		mtime,
		hdr.Size,
		hdr.Devmajor, // major
		hdr.Devminor, // minor
		hdr.RDevmajor,
		hdr.RDevminor,
		len(name)+1,          // +1 for terminating zero
		hdr.Check&0xffffffff) // check
	//	fmt.Printf("%d\n", hdr.ModTime.Unix())
	n, tw.err = tw.w.Write([]byte(shdr))
	if tw.err != nil {
		return tw.err
	}
	tw.length += int64(n)
	name = append(name, 0)
	n, tw.err = tw.w.Write(name)
	tw.length += int64(n)
	skip = (4 - (tw.length % 4)) % 4
	for ; skip > 0; skip-- {
		buf = append(buf, 0)
	}
	n, tw.err = tw.w.Write(buf)
	buf = nil
	tw.length += int64(n)
	tw.inode++
	tw.nb = int64(hdr.Size)
	return tw.err
}

// Write writes to the current entry in the tar archive.
// Write returns the error ErrWriteTooLong if more than
// hdr.Size bytes are written after WriteHeader.
func (tw *Writer) Write(b []byte) (n int, err error) {
	if tw.closed {
		err = ErrWriteAfterClose
		return
	}
	if int64(len(b)) > tw.nb {
		b = b[0:tw.nb]
	}
	n, werr := tw.w.Write(b)
	if werr != nil {
		return n, werr
	}
	tw.nb -= int64(n)
	tw.length += int64(n)
	tw.err = err
	return
}

// Close closes the tar archive, flushing any unwritten
// data to the underlying writer.
func (tw *Writer) Close() error {
	if tw.err != nil || tw.closed {
		return tw.err
	}
	//	tw.Flush()
	hdr := &Header{Name: "TRAILER!!!"}
	tw.err = tw.WriteHeader(hdr)
	if tw.err != nil {
		return tw.err
	}

	skip := (512 - (tw.length % 512)) % 512
	var buf []byte
	for ; skip > 0; skip-- {
		buf = append(buf, 0)
	}
	_, tw.err = tw.w.Write(buf)
	if tw.err != nil {
		return tw.err
	}
	tw.closed = true
	return tw.err
}
