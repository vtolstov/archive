package cpio

import (
	"fmt"
	"io"
	"os"
	"testing"
	"time"
)

func TestWriter(t *testing.T) {
	f, err := os.Create("testdata/test-new.cpio")
	if err != nil {
		t.Fatalf(err.Error())
	}
	defer f.Close()
	//	defer os.Remove("testdata/test-new.cpio")
	w := NewWriter(f)
	// 2013-06-11 16:23:51 +0400 MSK
	const form = "2006-01-02T15:04:05 -0700"
	tm, err := time.Parse(form, "2013-06-17T12:36:51 +0400")
	if err != nil {
		t.Fatalf(err.Error())
	}

	hdr := &Header{Magic: []byte("070701"), Name: "test.txt", Mode: 420, Uid: 1000, Gid: 1000, Nlink: 1, Typeflag: 010, Size: 5, ModTime: tm}

	//	fmt.Printf("%+v\n\n", hdr)
	err = w.WriteHeader(hdr)
	if err != nil {
		t.Fatalf(err.Error())
	}
	_, err = w.Write([]byte("test\n"))
	if err != nil {
		t.Fatalf(err.Error())
	}
	err = w.Close()
	if err != nil {
		t.Fatalf(err.Error())
	}
	f.Close()
	f, err = os.Open("testdata/test-new.cpio")
	if err != nil {
		t.Fatalf(err.Error())
	}
	defer f.Close()

	r := NewReader(f)

	for {
		hdr, err := r.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			//			fmt.Printf("%+v\n", hdr)
			t.Fatalf(err.Error())
		}

		switch hdr.Typeflag {

		default:
			//			fmt.Printf("%+v\n", hdr)
		}
	}
	//os.Remove("testdata/test-new.cpio")
}

func TestAll(t *testing.T) {
	r, err := os.Open("testdata/test-big.cpio")
	if err != nil {
		t.Fatalf(err.Error())
	}
	defer r.Close()
	w, err := os.Create("testdata/test-new.cpio")
	if err != nil {
		t.Fatal(err.Error())
	}
	//defer w.Close()
	cr := NewReader(r)
	cw := NewWriter(w)
	for {
		hdr, err := cr.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			t.Fatalf(err.Error())
		}
		err = cw.WriteHeader(hdr)
		if err != nil {
			t.Fatalf(err.Error())
		}
		fmt.Printf("%+v\n", hdr)
		//if hdr.Size != 0 {
		_, err = io.Copy(cw, cr)
		if err != nil {
			t.Fatalf(err.Error())
		}

	}
	cw.Close()
	w.Close()
	f, err := os.Open("testdata/test-new.cpio")
	if err != nil {
		t.Fatalf(err.Error())
	}
	cr = NewReader(f)
	for {
		hdr, err := cr.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			t.Fatalf(err.Error())
		}
		fmt.Printf("%+v\n", hdr)
	}
}
